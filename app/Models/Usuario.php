<?php


namespace curp;

class Usuario extends Conexion
{

    public $primer_nombre;
    public $segundo_nombre;
    public $apellido_paterno;
    public $apellido_materno;
    public $genero;
    public $edad;



    public function __construct()
    {
        parent::__construct();
    }

    function registrarUsuario(){
        $preparar = mysqli_prepare($this->conexion, "INSERT INTO usuario(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,genero,edad) 
        VALUES  (?,?,?,?,?,?)");
        $preparar->bind_param("sssssi", $this->primer_nombre,$this->segundo_nombre, $this->apellido_paterno,$this->apellido_materno,
            $this->genero,$this->edad);
        $preparar->execute();
    }

    static function mostrar(){
        $me = new Conexion();
        $preparar = mysqli_prepare($me->conexion, "SELECT * FROM usuario");
        $preparar->execute();
        $resultado = $preparar->get_result();
        while ($y=mysqli_fetch_assoc($resultado)){
            $t[]=$y;
        }
        return $t;
    }

    function actualizarDatos($dato){
        $preparar = mysqli_prepare($this->conexion, "UPDATE usuario SET primer_nombre=?, segundo_nombre=?,
        apellido_paterno=?, apellido_materno=?, genero=?, edad=? WHERE id_usuario = ?");
        $preparar->bind_param("sssssii", $this->primer_nombre,$this->segundo_nombre,$this->apellido_paterno,
            $this->apellido_materno,$this->genero,$this->edad,$dato);
        $preparar->execute();
    }

    static function eliminarRegistro($dato){
        $me= new Conexion();
        $preparar = mysqli_prepare($me->conexion, "DELETE FROM usuario WHERE  id_usuario = ?");
        $preparar->bind_param("i", $dato);
        $preparar->execute();
    }

    static function registrarTarea($dato,$nombreTarea){
        $me= new Conexion();
        $preparar = mysqli_prepare($me->conexion, "INSERT INTO `tareas_usuario`(`id_usuario`, `nombre_tarea`) 
        VALUES  (?,?)");
        $preparar->bind_param("is", $dato,$nombreTarea);
        $preparar->execute();
    }




}
<?php

namespace curp;

class Conexion
{
    public $conexion;
    public function __construct()
    {
        $host = "localhost";
        $user = "root";
        $pass = "";
        $bd = "lista_tareas"; // <-- nombre de la bd
        $this->conexion = mysqli_connect($host, $user, $pass, $bd);
        mysqli_query($this->conexion,"SET NAMES 'utf8");

    }

}
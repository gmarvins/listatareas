<?php

require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
use curp\Usuario;
use curp\Conexion;

class PersonaController
{

    public function __construct()
    {

    }
    function registrarUsuario(){

        $registro = new Usuario();

        $primerNombre = $_GET["primerNombre"];
        $segundoNombre = $_GET["segundoNombre"];
        $primerApellido = $_GET["primerApellido"];
        $segundoApellido = $_GET["segundoApellido"];
        $genero = $_GET["genero"];
        $edad = $_GET["edad"];
        $registro->primer_nombre = $primerNombre;
        $registro->segundo_nombre = $segundoNombre;
        $registro->apellido_paterno = $primerApellido;
        $registro->apellido_materno = $segundoApellido;
        $registro->genero = $genero;
        $registro->edad = $edad;
        $registro->registrarUsuario();

    }

    function mostrar(){

        $arreglo = Usuario::mostrar();
        echo json_encode($arreglo);

    }

    function actualizarDatos(){

        $actualiza = new Usuario();

        $primerNombre = $_GET["primerNombre"];
        $segundoNombre = $_GET["segundoNombre"];
        $primerApellido = $_GET["primerApellido"];
        $segundoApellido = $_GET["segundoApellido"];
        $genero = $_GET["genero"];
        $edad = $_GET["edad"];
        $actualiza->primer_nombre = $primerNombre;
        $actualiza->segundo_nombre = $segundoNombre;
        $actualiza->apellido_paterno = $primerApellido;
        $actualiza->apellido_materno = $segundoApellido;
        $actualiza->genero = $genero;
        $actualiza->edad = $edad;
        $actualiza->actualizarDatos(6);

    }

    function eliminarRegistro(){

        Usuario::eliminarRegistro(6);

    }
    function agregarTareaAlumno(){

        Usuario::registrarTarea(7,"Tarea Matematicas : FUNCIONES");

    }





}